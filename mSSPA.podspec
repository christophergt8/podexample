Pod::Spec.new do |s|

  s.name         = "mSSPA"
  s.version      = "1.0.0"
  s.summary      = "A short description of CocoaExample."

  s.homepage     = "http://EXAMPLE/CocoaExample"
  s.license      = "MIT"
  s.author       = { "Christopher Ganfornina Triguero" => "christopher.triguero@babel.es" }
  s.source       = { :git => "https://christophergt8@bitbucket.org/christophergt8/podexample.git", :tag => "#{s.version}" }
  s.source_files = "mSSPA", "mSSPA/**/*.{h,m,swift}"

  s.dependency "Alamofire"
  s.dependency "KeychainAccess"

end
