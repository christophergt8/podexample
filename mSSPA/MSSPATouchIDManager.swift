//
//  MSSPATouchIDManager.swift
//  mSSPA v02r01
//
//  Created by Karim Razak Soriano on 23/10/2018.
//  Copyright © 2018 Babel. All rights reserved.
//

import Foundation
import  LocalAuthentication

@objc public protocol MSSPATouchIDManagerDelegate {
    func userDidCheckWithValue(code: MSSPATouchIDCode)
}

@objc public enum MSSPATouchIDCode: Int {

    case okCode = 0
    case cancelCode
    case fallbackCode
    case manyErrorsCode
    case manyUndefinedCode
    case allowedCode
    case notAllowedCode
    case notConfigureCode
    case notFingersSetCode
}

@objc public class MSSPATouchIDManager: NSObject {

    var myContext: LAContext = LAContext()
    var myLocalizedReasonString: String = ""
    var delegate: MSSPATouchIDManagerDelegate?

    @objc public static let sharedInstance = MSSPATouchIDManager()

    @objc public func isTouchIdAvailable() -> Bool {
        self.myContext = LAContext()

        var authError: NSError?
        if self.myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError)  {
            return true
        }

        guard let error = authError else {return true}

        if #available(iOS 11.0, *) {
            if error.code == LAError.biometryNotAvailable.rawValue{
                return false
            }
        }

        switch error.code {
        case LAError.touchIDNotAvailable.rawValue:
            return false

        case -1000: // Simulator
            return false

        default:
            if (MSSPAUtils.getModelDevice() == "iPhone X") {
                return false
            } else {
                return true
            }
        }
    }

    @objc public func isTouchIdEnrolled() -> Bool {
        self.myContext = LAContext()

        var authError: NSError?
        if self.myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError)  {
            return true
        }
        guard let error = authError else {return true}

        if #available(iOS 11.0, *) {
            if error.code == LAError.biometryNotEnrolled.rawValue{
                return false
            }
        }
        switch error.code {
        case LAError.touchIDNotEnrolled.rawValue:
            return false
        default:
            return true
        }
    }

    @objc public func checkUserByTouchIDWithText(_ title: String, from delegate: MSSPATouchIDManagerDelegate) {

        self.delegate = delegate
        self.myContext = LAContext()
        self.myContext.localizedFallbackTitle = ""
        self.myLocalizedReasonString = title

        var authError: NSError?
        if self.myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {

            self.myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: self.myLocalizedReasonString) { (success, error) in

                if success{
                    DispatchQueue.main.async {
                        self.delegate?.userDidCheckWithValue(code: .okCode)
                    }
                }else{
                    guard let error = authError else {return}
                    DispatchQueue.main.async {
                        self.checkErrorCode(error.code)
                    }
                }
            }

        }else{
            DispatchQueue.main.async {
                self.delegate?.userDidCheckWithValue(code: .notAllowedCode)
            }
        }
    }

    @objc public func checkErrorCode(_ errorCode: Int) {

        if #available(iOS 11.0, *) {
            if errorCode == LAError.biometryNotEnrolled.rawValue{
                self.delegate?.userDidCheckWithValue(code: .notFingersSetCode)
                return
            }
            if errorCode == LAError.biometryNotAvailable.rawValue{
                self.delegate?.userDidCheckWithValue(code: .notAllowedCode)
                return
            }
        }

        switch errorCode {
        case LAError.authenticationFailed.rawValue:
            self.delegate?.userDidCheckWithValue(code: .manyErrorsCode)
            break;
        case LAError.userCancel.rawValue:
            self.delegate?.userDidCheckWithValue(code: .cancelCode)
            break;
        case LAError.userFallback.rawValue:
            self.delegate?.userDidCheckWithValue(code: .fallbackCode)
            break;
        case LAError.systemCancel.rawValue:
            self.delegate?.userDidCheckWithValue(code: .cancelCode)
            break;
        case LAError.passcodeNotSet.rawValue:
            self.delegate?.userDidCheckWithValue(code: .notConfigureCode)
            break;
        case LAError.touchIDNotAvailable.rawValue:
            self.delegate?.userDidCheckWithValue(code: .notAllowedCode)
            break;
        case LAError.touchIDNotEnrolled.rawValue:
            self.delegate?.userDidCheckWithValue(code: .notFingersSetCode)
            break;
        default:
            self.delegate?.userDidCheckWithValue(code: .manyUndefinedCode)
            break;
        }
    }
    
}
