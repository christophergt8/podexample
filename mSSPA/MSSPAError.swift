//
//  MSSPAError.swift
//  mSSPA v02r01
//
//  Created by Karim Razak Soriano on 23/10/2018.
//  Copyright © 2018 Babel. All rights reserved.
//

import Foundation

@objc public class MSSPAError: NSObject {

    @objc public var status: Int = 0
    @objc public var type: String = ""
    @objc public var code: String = ""
    @objc public var message: String = ""

    public init(status: Int, type: String, code: String, message: String) {

        self.status = status
        self.type = type
        self.code = code
        self.message = message
    }
}
