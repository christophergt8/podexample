//
//  MSSPAWebServicesManager.swift
//  mSSPA v02r01
//
//  Created by Karim Razak Soriano on 24/10/2018.
//  Copyright © 2018 Babel. All rights reserved.
//

import Foundation
import Alamofire

//MARK: - Alias
public typealias ServiceAuthenticationGetTokenWebDesktopCallback = (_ token: MSSPAToken?, _ error: MSSPAError?) -> Void
public typealias ServiceAuthenticationVerifyTokenCallback = (_ status: Bool) -> Void
public typealias ServiceAuthenticationRemoveTokenCallback = (_ status: Bool) -> Void
public typealias ServiceAuthenticationGetConfigAuthenticationCallback = (_ lanzadora: Bool, _ certificado: Bool, _ dnie: Bool, _ clave: Bool, _ webDesktop: Bool, _ errorMSSPA: MSSPAError?) -> Void
public typealias ServiceIdDeviceMsspaGenerateIdentifierCallback = (_ idDeviceMsspa: String?, _ errorMSSPA: MSSPAError?) -> Void
public typealias ServiceOctopushRegisterDeviceCallBack = ( _ errorMSSPA: MSSPAError?) -> Void
public typealias ServiceOctopushRemoveRegisterDeviceCallBack = ( _ errorMSSPA: MSSPAError?) -> Void
public typealias ServiceOctopushSendAckReadPushCallBack = ( _ errorMSSPA: MSSPAError?) -> Void
public typealias ServiceOctopushGetMessagesCallBack = (_ listMessages: Array<Any>?, _ errorMSSPA: MSSPAError?) -> Void
public typealias ServiceOctopushUpdateMessageCallBack = ( _ errorMSSPA: MSSPAError?) -> Void
public typealias ServiceUsersGetBasicUserDataCallBack = (_ name :String?, _ surname1 :String?, _ surname2 :String?, _ birthdate :String?, _ nuhsa :String?, _ idSex :Int?, _ errorMSSPA: MSSPAError?) -> Void
public typealias ServiceManagePINRegisterPINCallback = ( _ errorMSSPA: MSSPAError?) -> Void
public typealias ServiceManagePINVerifyPINCallback = (_ valido: Bool, _ errorMSSPA: MSSPAError?) -> Void
public typealias ServiceManagePINEditPINCallback = ( _ errorMSSPA: MSSPAError?) -> Void

@objc public class MSSPAWebServicesManager: NSObject {

    // MARK: - Constants
    var MSSPA_DEMO_MODE = false
    var MSSPA_PRO_MODE = false

    let MSSPA_INTERNET_TIMEOUT = 30.0 // seconds

    let MSSPA_ERROR_NETWORK_CONNECTION = 1001
    let MSSPA_ERROR_TECHNICAL = 1002

    let ID_PLATFORM = "1"

    let MSSPA_HOST_1_DES = "https://www.juntadeandalucia.es/servicioandaluzdesalud/msspa_gateway_pre"
    let MSSPA_HOST_1_PRO = "https://www.juntadeandalucia.es/servicioandaluzdesalud/msspa_gateway"
    let MSSPA_HOST_2_DES = "https://ws237.juntadeandalucia.es"
    let MSSPA_HOST_2_PRO = "https://ws238.juntadeandalucia.es:8442"

    let MSSPA_URL_SW_AUTHENTICATION_GET_TOKENS_CERTIFICATE = "/api/v1.0/autenticacion/token?tipo=afirma"
    let MSSPA_URL_SW_AUTHENTICATION_GET_TOKENS_CLAVE = "/api/v1.0/autenticacion/token?tipo=proxyClave"
    let MSSPA_URL_SW_AUTHENTICATION_GET_TOKENS_WEB_DESKTOP = "/api/v1.0/autenticacion/token?tipo=otp"
    let MSSPA_URL_SW_AUTHENTICATION_VERIFY_TOKEN = "/api/v1.0/autenticacion/token/estado?"
    let MSSPA_URL_SW_AUTHENTICATION_REMOVE_TOKEN = "/api/v1.0/autenticacion/token?"
    let MSSPA_URL_SW_AUTHENTICATION_GET_CONFIG_AUTHEN = "/api/v1.0/autenticacion?"

    let MSSPA_URL_SW_IDDEVICEMSSPA_GENERATE_IDENTIFIER = "/api/v1.0/dispositivos/identificadores?"

    let MSSPA_URL_SW_OCTOPUSH_REGISTER_DEVICE = "/api/v1.0/dispositivos?"
    let MSSPA_URL_SW_OCTOPUSH_REMOVE_REGISTER_DEVICE = "/api/v1.0/dispositivos/"
    let MSSPA_URL_SW_OCTOPUSH_SEND_ACK_READ_PUSH = "/api/v1.0/dispositivos/"
    let MSSPA_URL_SW_OCTOPUSH_GET_MESSAGES = "/api/v1.0/dispositivos/"
    let MSSPA_URL_SW_OCTOPUSH_UPDATE_MESSAGE = "/api/v1.0/dispositivos/"

    let MSSPA_URL_SW_USERS_GET_BASIC_USER_DATA = "/api/v1.0/usuarios/demograficos?completa=false"

    let MSSPA_URL_SW_MANAGEPIN_REGISTER  = "/api/v1.0/usuarios/pin?"
    let MSSPA_URL_SW_MANAGEPIN_VERIFY  = "/api/v1.0/usuarios/pin$validar?"
    let MSSPA_URL_SW_MANAGEPIN_EDIT  = "/api/v1.0/usuarios/pin?"

    let MSSPA_URL_SW_PARAM_CLIENT_ID = "clientId=54f0c455-4d80-421f-82ca-9194df24859d"
    let MSSPA_URL_SW_PARAM_REDIRECT_URI = "redirectURI="
    let MSSPA_URL_SW_PARAM_REDIRECT_URI_2 = "%3A%2F%2Fautentication"
    let MSSPA_URL_SW_PARAM_APIKEY = "apikey="
    let MSSPA_URL_SW_PARAM_REFRESH_TOKEN = "refreshToken="
    let MSSPA_URL_SW_PARAM_ID_DEVICE = "idDevice="
    let MSSPA_URL_SW_PARAM_ID_DEVICE_MSSPA = "idDeviceMsspa="
    let MSSPA_URL_SW_PARAM_APPKEY = "appKey="
    let MSSPA_URL_SW_PARAM_DNI = "dni="
    let MSSPA_URL_SW_PARAM_TOKEN_OTP = "tokenOTP="
    let MSSPA_URL_SW_PARAM_APPKEYOCTOPUSH = "appKeyOctopush="
    let MSSPA_URL_SW_PARAM_VALIDTRUE = "valid=true"
    let MSSPA_URL_SW_PARAM_LOCALE = "locale="
    let MSSPA_URL_SW_PARAM_HASH = "hash="
    let MSSPA_URL_SW_PARAM_IDSO = "idSo="

    @objc public static let sharedInstance = MSSPAWebServicesManager()

    /**
     Default value: false
     */
    @objc public func setDemoMode(_ demoMode: Bool) {
        self.MSSPA_DEMO_MODE = demoMode
    }

    /**
     Default value: false
     */
    @objc public func setProMode(_ proMode: Bool) {
        self.MSSPA_PRO_MODE = proMode
    }

    func getHost(_ num: Int) -> String {
        var result = "";

        if (MSSPA_PRO_MODE) {

            if (num == 1) {
                result = MSSPA_HOST_1_PRO;
            } else if (num == 2) {
                result = MSSPA_HOST_2_PRO;
            }

        } else {

            if (num == 1) {
                result = MSSPA_HOST_1_DES;
            } else if (num == 2) {
                result = MSSPA_HOST_2_DES;
            }
        }

        return result;
    }

    func getErrorNetworkConnection() -> MSSPAError {
        return MSSPAError(status: MSSPA_ERROR_NETWORK_CONNECTION, type: "MSSPA Library", code: "NC01", message: "No hay conexión con el servidor")
    }

    func getErrorMSSPA(error: Any?) -> MSSPAError {

        guard let errorDic = error as? Dictionary<String, Any> else {
            return MSSPAError(status: MSSPA_ERROR_TECHNICAL, type: "MSSPA Library", code: "TE01", message: "Ha ocurrido un error técnico")
        }

        if let error = errorDic["error"] as? Dictionary<String, Any>,
            let status = error["status"] as? Int,
            let type = error["type"] as? String,
            let code = error["code"] as? String,
            let message = error["message"] as? String {

            return MSSPAError(status: status, type: type, code: code, message: message)

        }else {
            return MSSPAError(status: MSSPA_ERROR_TECHNICAL, type: "MSSPA Library", code: "TE01", message: "Ha ocurrido un error técnico")
        }
    }

    @objc public func createRequest(url: URL, httpMethod: String, appKey: String, idDevice: String) -> URLRequest {
        var mutableRequest = URLRequest(url: url)
        mutableRequest.httpMethod = httpMethod

        // objective-C: createManagerWithAppKey
        mutableRequest.timeoutInterval = MSSPA_INTERNET_TIMEOUT
        mutableRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        mutableRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        mutableRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        mutableRequest.setValue(appKey, forHTTPHeaderField: "appKey")
        mutableRequest.setValue(idDevice, forHTTPHeaderField: "idDevice")

        return mutableRequest
    }

    /***
     API Autentication v00r29

     This method opens an url to authenticate through digital certificate, the app will be waiting to get the response from a defined url
     */
    @objc public func getAuthenticationUrlCertificateWithApiKey(_ apiKey: String, withScheme: String, withIdDevice: String, withIdDeviceMsspa: String, withAppKey: String) -> String {

        let url = String(format: "%@%@&%@&%@%@%@&%@%@&%@%@&%@%@&%@%@", arguments: [getHost(2), MSSPA_URL_SW_AUTHENTICATION_GET_TOKENS_CERTIFICATE, MSSPA_URL_SW_PARAM_CLIENT_ID, MSSPA_URL_SW_PARAM_REDIRECT_URI, withScheme, MSSPA_URL_SW_PARAM_REDIRECT_URI_2, MSSPA_URL_SW_PARAM_APIKEY, apiKey, MSSPA_URL_SW_PARAM_ID_DEVICE, withIdDevice, MSSPA_URL_SW_PARAM_ID_DEVICE_MSSPA, withIdDeviceMsspa, MSSPA_URL_SW_PARAM_APPKEY, withAppKey])
        return url
    }

    /***
     This method opens an url to authenticate through Cl@ve, the app will be waiting to get the response from a defined url
     */
    @objc public func getAuthenticationUrlClaveWithApiKey(_ apiKey: String, withScheme: String, withIdDevice: String, withIdDeviceMsspa: String, withAppKey: String) -> String {

        let url = String(format: "%@%@&%@%@%@&%@%@&%@%@&%@%@&%@%@", arguments: [getHost(1), MSSPA_URL_SW_AUTHENTICATION_GET_TOKENS_CLAVE, MSSPA_URL_SW_PARAM_REDIRECT_URI, withScheme, MSSPA_URL_SW_PARAM_REDIRECT_URI_2, MSSPA_URL_SW_PARAM_APIKEY, apiKey, MSSPA_URL_SW_PARAM_ID_DEVICE, withIdDevice, MSSPA_URL_SW_PARAM_ID_DEVICE_MSSPA, withIdDeviceMsspa, MSSPA_URL_SW_PARAM_APPKEY, withAppKey])
        return url
    }

    /***
     This service gets the authentication response when an user tries logged through web/desktop
     */
    @objc public func serviceAuthenticationGetTokenWebDesktopWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, tokenOTP: String, hashDNI: String, withCallback: @escaping ServiceAuthenticationGetTokenWebDesktopCallback) -> String {

        if MSSPA_DEMO_MODE {
            let tokenMSSPA = MSSPAToken(accessToken: "c7d82733-1827-465c-a278-01f83919e467", tokenType: "Bearer", expiresIn: "3600", scope: "ciudadano", pin: true)

            withCallback(tokenMSSPA, nil)
        }else if !MSSPAUtils.hasNetworkConnection() {
            let errorMSSPA = getErrorNetworkConnection()
            withCallback(nil, errorMSSPA)
        } else{

            let urlStr = String(format: "%@%@&%@%@&%@%@&%@%@&%@%@&%@%@", arguments: [getHost(2), MSSPA_URL_SW_AUTHENTICATION_GET_TOKENS_WEB_DESKTOP, MSSPA_URL_SW_PARAM_APIKEY, apiKey, MSSPA_URL_SW_PARAM_ID_DEVICE, withIdDevice, MSSPA_URL_SW_PARAM_DNI, hashDNI, MSSPA_URL_SW_PARAM_TOKEN_OTP, tokenOTP, MSSPA_URL_SW_PARAM_APPKEY, withAppKey])

            guard let url = URL(string: urlStr) else {return ""}

            var mutableRequest = createRequest(url: url, httpMethod: "GET", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")

            Alamofire.request(mutableRequest).responseJSON { response in

                switch response.result {
                case .success(let json):
                    if let json = json as? Dictionary<String, Any> {

                        let accessToken = json["accessToken"] as? String ?? ""
                        let tokenType = json["tokenType"] as? String ?? ""
                        let expiresIn = json["expiresIn"] as? String ?? ""
                        let scope = json["scope"] as? String ?? ""
                        let hasPIN = json["pin"] as? Bool ?? false

                        let tokenMSSPA = MSSPAToken(accessToken: accessToken, tokenType: tokenType, expiresIn: expiresIn, scope: scope, pin: hasPIN)

                        withCallback(tokenMSSPA, nil)
                    }else{
                        let errorMSSPA = self.getErrorNetworkConnection()
                        withCallback(nil, errorMSSPA)
                    }
                case .failure(_):
                    withCallback(nil, self.getErrorMSSPA(error: response.result.value))
                }
            }


        }
        return ""
    }

    /***
     This service verifies the expiration date of the access token
     */
    @objc public func serviceAuthenticationVerifyTokenWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, withAuthorization: String, withCallback: @escaping ServiceAuthenticationVerifyTokenCallback){

        if MSSPA_DEMO_MODE {
            withCallback(true)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(false)
        } else{

            let urlStr = String(format: "%@%@%@%@", arguments: [getHost(1), MSSPA_URL_SW_AUTHENTICATION_VERIFY_TOKEN, MSSPA_URL_SW_PARAM_APIKEY, apiKey])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "GET", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")
            mutableRequest.setValue(withAuthorization, forHTTPHeaderField: "Authorization")

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(let json):
                    if let json = json as? Dictionary<String, Any> {
                        let status = json["status"] as? Bool ?? false
                        withCallback(status)
                    }else{
                        withCallback(false)
                    }
                case .failure(_):
                    withCallback(false)
                }
            }
        }
    }


    /***
     This service removes the access token
     */
    @objc public func serviceAuthenticationRemoveTokenWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, withAuthorization: String, withCallback: @escaping ServiceAuthenticationRemoveTokenCallback){

        if MSSPA_DEMO_MODE {
            withCallback(true)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(false)
        } else{

            let urlStr = String(format: "%@%@%@%@", arguments: [getHost(1), MSSPA_URL_SW_AUTHENTICATION_REMOVE_TOKEN, MSSPA_URL_SW_PARAM_APIKEY, apiKey])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "DELETE", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")
            mutableRequest.setValue(withAuthorization, forHTTPHeaderField: "Authorization")

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(_):
                    withCallback(true)
                case .failure(_):
                    withCallback(false)
                }
            }
        }
    }


    /***
     This service gets the configuration of the methods of authentication
     */
    @objc public func serviceAuthenticationGetConfigAuthenticationWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, withCallback: @escaping ServiceAuthenticationGetConfigAuthenticationCallback){

        if MSSPA_DEMO_MODE {
            withCallback(true, true, true, true, true, nil)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(false, false, false, false, false, getErrorNetworkConnection())
        } else{

            let urlStr = String(format: "%@%@%@%@&%@%@&%@%@", arguments: [getHost(1), MSSPA_URL_SW_AUTHENTICATION_GET_CONFIG_AUTHEN, MSSPA_URL_SW_PARAM_APIKEY, apiKey, MSSPA_URL_SW_PARAM_APPKEY, withAppKey, MSSPA_URL_SW_PARAM_IDSO, ID_PLATFORM])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "GET", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(let json):
                    if let json = json as? Dictionary<String, Any>,
                        let operaciones = json["operaciones"] as? Dictionary<String, Any>{
                        let lanzadora = json["lanzadora"] as? Bool ?? false
                        let certificado = operaciones["certificado"] as? Bool ?? false
                        let dnie = operaciones["dnie"] as? Bool ?? false
                        let clave = operaciones["clave"] as? Bool ?? false
                        let webDesktop = operaciones["otp"] as? Bool ?? false

                        withCallback(lanzadora, certificado, dnie, clave, webDesktop, nil)
                    }else{
                        withCallback(false, false, false, false, false, nil)
                    }
                case .failure(_):
                    withCallback(false, false, false, false, false, self.getErrorMSSPA(error: response.result.value))
                }
            }
        }
    }


    /***
     API IdDeviceMSSPA v00r10
     This service generates a MSSPA device identifier
     */
    @objc public func serviceIdDeviceMsspaGenerateIdentifierWithApiKey(_ apiKey: String, withIdDevice: String, withAppKey: String, withCallback: @escaping ServiceIdDeviceMsspaGenerateIdentifierCallback){

        if MSSPA_DEMO_MODE {
            withCallback("0123456789", nil)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(nil, getErrorNetworkConnection())
        } else{

            let urlStr = String(format: "%@%@%@%@", arguments: [getHost(1), MSSPA_URL_SW_IDDEVICEMSSPA_GENERATE_IDENTIFIER, MSSPA_URL_SW_PARAM_APIKEY, apiKey])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "GET", appKey: withAppKey, idDevice: withIdDevice)

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(let json):
                    if let json = json as? Dictionary<String, Any> {
                        let idDeviceMsspa = json["idDeviceMsspa"] as? String ?? ""
                        withCallback(idDeviceMsspa, nil)
                    }else{
                        withCallback(nil, nil)
                    }
                case .failure(_):
                    withCallback(nil, self.getErrorMSSPA(error: response.result.value))
                }
            }
        }
    }

    /***
     API Octopush v00r32
     This service registers the device in Octopush
     */
    @objc public func serviceOctopushRegisterDeviceWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, appKeyOctopush: String, authorization: String, idNotification: String, migratedFCM: Bool, migratedApple: Bool, versionApp: String, locale: String, withCallback: @escaping ServiceOctopushRegisterDeviceCallBack){

        if MSSPA_DEMO_MODE {
            withCallback(nil)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(getErrorNetworkConnection())
        } else{

            let urlStr = String(format: "%@%@%@%@", arguments: [getHost(1), MSSPA_URL_SW_OCTOPUSH_REGISTER_DEVICE, MSSPA_URL_SW_PARAM_APIKEY, apiKey])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "POST", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")
            mutableRequest.setValue(authorization, forHTTPHeaderField: "Authorization")
            mutableRequest.setValue(appKeyOctopush, forHTTPHeaderField: "appKeyOctopush")
            mutableRequest.setValue(ID_PLATFORM, forHTTPHeaderField: "deviceType")
            mutableRequest.setValue(idNotification, forHTTPHeaderField: "idNotification")
            mutableRequest.setValue(MSSPAUtils.getModelDevice(), forHTTPHeaderField: "model")
            mutableRequest.setValue(UIDevice.current.systemVersion, forHTTPHeaderField: "versionSO")
            mutableRequest.setValue(NSNumber(value:migratedFCM).stringValue, forHTTPHeaderField: "migratedFCM")
            mutableRequest.setValue(NSNumber(value:migratedApple).stringValue, forHTTPHeaderField: "migratedApple")
            mutableRequest.setValue(appKeyOctopush, forHTTPHeaderField: "appKeyOctopush")
            mutableRequest.setValue(withIdDevice, forHTTPHeaderField: "idDevice")
            mutableRequest.setValue(locale, forHTTPHeaderField: "locale")
            mutableRequest.setValue(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? "", forHTTPHeaderField: "versionApp")

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(_):
                    withCallback(nil)
                case .failure(_):
                    withCallback(self.getErrorMSSPA(error: response.result.value))
                }
            }
        }
    }

    /***
     This service removes one device registered in Octopush
     */
    @objc public func serviceOctopushRemoveRegisterDeviceWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, appKeyOctopush: String, withCallback: @escaping ServiceOctopushRemoveRegisterDeviceCallBack){

        if MSSPA_DEMO_MODE {
            withCallback(nil)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(getErrorNetworkConnection())
        } else{

            let urlStr = String(format: "%@%@%@?%@%@&%@%@", arguments: [getHost(1), MSSPA_URL_SW_OCTOPUSH_REMOVE_REGISTER_DEVICE, withIdDevice, MSSPA_URL_SW_PARAM_APIKEY, apiKey, MSSPA_URL_SW_PARAM_APPKEYOCTOPUSH, appKeyOctopush])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "DELETE", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")
            mutableRequest.setValue(appKeyOctopush, forHTTPHeaderField: "appKeyOctopush")

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(_):
                    withCallback(nil)
                case .failure(_):
                    withCallback(self.getErrorMSSPA(error: response.result.value))
                }
            }
        }
    }


    /***
    This service sends a push notification ACK to Octopush
     */
    @objc public func serviceOctopushSendAckReadPushWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, appKeyOctopush: String, idMessage: String, withCallback: @escaping ServiceOctopushSendAckReadPushCallBack){

        if MSSPA_DEMO_MODE {
            withCallback(nil)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(getErrorNetworkConnection())
        } else{

            let urlStr = String(format: "%@%@%@/notificaciones/%@?%@%@&%@%@", arguments: [getHost(1), MSSPA_URL_SW_OCTOPUSH_SEND_ACK_READ_PUSH, withIdDevice, idMessage, MSSPA_URL_SW_PARAM_APIKEY, apiKey, MSSPA_URL_SW_PARAM_APPKEYOCTOPUSH, appKeyOctopush])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "PUT", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")
            mutableRequest.setValue(appKeyOctopush, forHTTPHeaderField: "appKeyOctopush")

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(_):
                    withCallback(nil)
                case .failure(_):
                    withCallback(self.getErrorMSSPA(error: response.result.value))
                }
            }
        }
    }


    /***
     This service gets messages from Octopush
     */
    @objc public func serviceOctopushGetMessagesWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, appKeyOctopush: String, withAuthorization: String, locale: String, withCallback: @escaping ServiceOctopushGetMessagesCallBack){

        if MSSPA_DEMO_MODE {

            let messageMSSPA1 = MSSPAMessage(idMessage: "1", sendPush: false, pushType: "", pushAction: "", pushInvisible: false, pushSilent: false, dateStart: "20170901000000", dateEnd: "20180101000000", messageTitle: "Message 1", messageDescription: "Description message 1", messageUrl: "http://www.google.es", messageLocale: "es", messageReaded: false)

            let messageMSSPA2 = MSSPAMessage(idMessage: "2", sendPush: false, pushType: "", pushAction: "", pushInvisible: false, pushSilent: false, dateStart: "20170901000000", dateEnd: "20180101000000", messageTitle: "Message 2", messageDescription: "Description message 2", messageUrl: "http://www.google.es", messageLocale: "es", messageReaded: false)

            withCallback([messageMSSPA1, messageMSSPA2], nil)

        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(nil, getErrorNetworkConnection())
        } else{

            let urlStr = String(format: "%@%@%@/notificaciones/rich?%@%@&%@%@&%@&%@%@", arguments: [getHost(1), MSSPA_URL_SW_OCTOPUSH_GET_MESSAGES, withIdDevice, MSSPA_URL_SW_PARAM_APIKEY, apiKey, MSSPA_URL_SW_PARAM_APPKEYOCTOPUSH, appKeyOctopush, MSSPA_URL_SW_PARAM_VALIDTRUE, MSSPA_URL_SW_PARAM_LOCALE, locale])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "GET", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")
            mutableRequest.setValue(appKeyOctopush, forHTTPHeaderField: "appKeyOctopush")
            mutableRequest.setValue(withAuthorization, forHTTPHeaderField: "Authorization")

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(let json):

                    if let listMessagesJSON = json as? Array<Any>{
                        var listMgs = [MSSPAMessage]()

                        listMessagesJSON.forEach{ list in
                            if let list = list as? Dictionary<String,Any>,
                                let richParams = list["richParams"] as? Dictionary<String,Any> {

                                var pushType = richParams["pushType"] as? String ?? ""
                                var pushAction = richParams["pushAction"] as? String ?? ""
                                let invisible = richParams["invisible"] as? String ?? ""
                                let silent = richParams["silent"] as? String ?? ""
                                let sendPush = richParams["sendPush"] as? Bool ?? false
                                let startDate = richParams["startDate"] as? String ?? ""
                                let endDate = richParams["endDate"] as? String ?? ""
                                let idMessage = richParams["idMessage"] as? String ?? ""
                                let contMessageRichDTO = richParams["contMessageRichDTO"] as? Dictionary<String,Any> ?? [:]
                                let description = contMessageRichDTO["description"] as? String ?? ""
                                let htmlContentURL = contMessageRichDTO["htmlContentURL"] as? String ?? ""
                                let locale = contMessageRichDTO["locale"] as? String ?? ""
                                let title = contMessageRichDTO["title"] as? String ?? ""
                                let readed = contMessageRichDTO["readed"] as? Bool ?? false

                                var bInvisible = false
                                var bSilent = false
                                if !sendPush {
                                    pushType = ""
                                    pushAction = ""
                                } else {
                                    if invisible == "true" {
                                        bInvisible = true
                                    }
                                    if silent == "true" {
                                        bSilent = true
                                    }
                                }

                                let messageMSSPA = MSSPAMessage(idMessage: idMessage, sendPush: sendPush, pushType: pushType, pushAction: pushAction, pushInvisible: bInvisible, pushSilent: bSilent, dateStart: startDate, dateEnd: endDate, messageTitle: title, messageDescription: description, messageUrl: htmlContentURL, messageLocale: locale, messageReaded: readed)

                                listMgs.append(messageMSSPA)
                            }
                        }

                        withCallback(listMgs, nil)

                    }else{
                        withCallback(nil, nil)
                    }
                case .failure(_):
                    withCallback(nil, self.getErrorMSSPA(error: response.result.value))
                }
            }
        }
    }

    /***
     This service updates the "read" param in an Octopush message
     */
    @objc public func serviceOctopushUpdateMessageWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, appKeyOctopush: String, withAuthorization: String, idMessage: String, locale: String, withCallback: @escaping ServiceOctopushUpdateMessageCallBack){

        if MSSPA_DEMO_MODE {
            withCallback(nil)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(getErrorNetworkConnection())
        } else{

            let urlStr = String(format: "%@%@%@/notificaciones/rich?%@%@&%@%@&%@%@", arguments: [getHost(1),MSSPA_URL_SW_OCTOPUSH_UPDATE_MESSAGE, withIdDevice, MSSPA_URL_SW_PARAM_APIKEY, apiKey, MSSPA_URL_SW_PARAM_APPKEYOCTOPUSH, appKeyOctopush, MSSPA_URL_SW_PARAM_LOCALE, locale])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "PUT", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withAuthorization, forHTTPHeaderField: "Authorization")
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")
            mutableRequest.setValue(appKeyOctopush, forHTTPHeaderField: "appKeyOctopush")

            let params: [String : Any] = ["readed": 1 , "listIdMessage": [idMessage]]
            mutableRequest.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(_):
                    withCallback(nil)
                case .failure(_):
                    withCallback(self.getErrorMSSPA(error: response.result.value))
                }
            }
        }
    }


    /***
     API Users v00r35
     This service gets the basic data of the user
     */
    public func serviceUsersGetBasicUserDataWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, withAuthorization: String, withCallback: @escaping ServiceUsersGetBasicUserDataCallBack){

        if MSSPA_DEMO_MODE {
            withCallback("Usuario", "Modo", "Demo", "19850209", "AN0000000000", 0, nil)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(nil, nil, nil, nil, nil, 0, getErrorNetworkConnection())
        } else{

            let urlStr = String(format: "%@%@&%@%@", arguments: [getHost(1), MSSPA_URL_SW_USERS_GET_BASIC_USER_DATA, MSSPA_URL_SW_PARAM_APIKEY, apiKey])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "GET", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")
            mutableRequest.setValue(withAuthorization, forHTTPHeaderField: "Authorization")

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(let json):

                    if let json = json as? Dictionary<String,Any>,
                        let administrativos = json["administrativos"] as? Dictionary<String,Any> {

                        let userIdSex = administrativos["idSexo"] as? Int ?? 0
                        let userNuhsa = administrativos["nuhsa"] as? String ?? ""
                        let userBirthdate = administrativos["fechaNacimiento"] as? String ?? ""
                        let userName = administrativos["nombre"] as? String ?? ""
                        let userSurname1 = administrativos["apellido1"] as? String ?? ""
                        let userSurname2 = administrativos["apellido2"] as? String ?? ""
                        withCallback(userName, userSurname1, userSurname2, userBirthdate, userNuhsa, userIdSex, nil)

                    }
                case .failure(_):
                    withCallback(nil, nil, nil, nil, nil, 0, self.getErrorMSSPA(error: response.result.value))
                }
            }
        }
    }

    /***
     This service registers a PIN in the mSSPA
     */
    @objc public func serviceManagePINRegisterPINWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, withAuthorization: String, withHashPIN: String, withCallback: @escaping ServiceManagePINRegisterPINCallback){

        if MSSPA_DEMO_MODE {
            withCallback(nil)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(getErrorNetworkConnection())
        } else{

            let urlStr = String(format: "%@%@%@%@&%@%@", arguments: [getHost(1), MSSPA_URL_SW_MANAGEPIN_REGISTER, MSSPA_URL_SW_PARAM_APIKEY, apiKey, MSSPA_URL_SW_PARAM_HASH, withHashPIN])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "POST", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")
            mutableRequest.setValue(withAuthorization, forHTTPHeaderField: "Authorization")

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(_):
                    withCallback(nil)
                case .failure(_):
                    withCallback(self.getErrorMSSPA(error: response.result.value))
                }
            }
        }
    }


    /***
     This service verifies the mSSPA PIN
     */
    @objc public func serviceManagePINVerifyPINWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, withAuthorization: String, withHashPIN: String, withCallback: @escaping ServiceManagePINVerifyPINCallback){

        if MSSPA_DEMO_MODE {
            withCallback(true, nil)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(false, getErrorNetworkConnection())
        } else{

            let urlStr = String(format: "%@%@%@%@&%@%@", arguments: [getHost(1), MSSPA_URL_SW_MANAGEPIN_VERIFY, MSSPA_URL_SW_PARAM_APIKEY, apiKey, MSSPA_URL_SW_PARAM_HASH, withHashPIN])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "POST", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")
            mutableRequest.setValue(withAuthorization, forHTTPHeaderField: "Authorization")

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(let json):

                    if let json = json as? Dictionary<String,Any> {
                        let valido = json["valido"] as? Bool ?? false
                        withCallback(valido, nil)
                    }else{
                        withCallback(false, nil)
                    }
                case .failure(let error):
                    withCallback(false, self.getErrorMSSPA(error: error))
                }
            }
        }
    }

    /***
     This service edits the mSSPA PIN
     */
    @objc public func serviceManagePINEditPINWithApiKey(_ apiKey: String, withIdDevice: String, withIdDeviceMSSPA: String, withAppKey: String, withAuthorization: String, withHashPIN: String, withCallback: @escaping ServiceManagePINEditPINCallback){

        if MSSPA_DEMO_MODE {
            withCallback(nil)
        }else if !MSSPAUtils.hasNetworkConnection() {
            withCallback(getErrorNetworkConnection())
        } else{

            let urlStr = String(format: "%@%@%@%@&%@%@", arguments: [getHost(1), MSSPA_URL_SW_MANAGEPIN_EDIT, MSSPA_URL_SW_PARAM_APIKEY, apiKey, MSSPA_URL_SW_PARAM_HASH, withHashPIN])
            guard let url = URL(string: urlStr) else {return }

            var mutableRequest = createRequest(url: url, httpMethod: "PUT", appKey: withAppKey, idDevice: withIdDevice)
            mutableRequest.setValue(withIdDeviceMSSPA, forHTTPHeaderField: "idDeviceMsspa")
            mutableRequest.setValue(withAuthorization, forHTTPHeaderField: "Authorization")

            Alamofire.request(mutableRequest).responseJSON { response in
                switch response.result {
                case .success(_):
                    withCallback(nil)
                case .failure(_):
                    withCallback(self.getErrorMSSPA(error: response.result.value))
                }
            }
        }
    }

}
