//
//  MSSPAToken.swift
//  mSSPA v02r01
//
//  Created by Karim Razak Soriano on 23/10/2018.
//  Copyright © 2018 Babel. All rights reserved.
//

import Foundation

@objc public class MSSPAToken: NSObject {

    var accessToken: String
    var tokenType: String
    var expiresIn: String
    var scope: String
    var pin: Bool

    public init(accessToken: String, tokenType: String, expiresIn: String, scope: String, pin: Bool) {
        self.accessToken = accessToken
        self.tokenType = tokenType
        self.expiresIn = expiresIn
        self.scope = scope
        self.pin = pin
    }
}
