//
//  MSSPAMessage.swift
//  mSSPA v02r01
//
//  Created by Karim Razak Soriano on 23/10/2018.
//  Copyright © 2018 Babel. All rights reserved.
//

import Foundation

@objc public class MSSPAMessage: NSObject {

    var idMessage: String
    var sendPush: Bool
    var pushType: String
    var pushAction: String
    var pushInvisible: Bool
    var pushSilent: Bool
    var dateStart: String
    var dateEnd: String
    var messageTitle: String
    var messageDescription: String
    var messageUrl: String
    var messageLocale: String
    var messageReaded: Bool

    public init(idMessage: String, sendPush: Bool, pushType: String, pushAction: String, pushInvisible: Bool, pushSilent: Bool, dateStart: String, dateEnd: String, messageTitle: String, messageDescription: String, messageUrl: String, messageLocale: String, messageReaded: Bool) {

        self.idMessage = idMessage
        self.sendPush = sendPush
        self.pushType = pushType
        self.pushAction = pushAction
        self.pushInvisible = pushInvisible
        self.pushSilent = pushSilent
        self.dateStart = dateStart
        self.dateEnd = dateEnd
        self.messageTitle = messageTitle
        self.messageDescription = messageDescription
        self.messageUrl = messageUrl
        self.messageLocale = messageLocale
        self.messageReaded = messageReaded
    }
}
