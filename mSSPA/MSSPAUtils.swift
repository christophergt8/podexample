//
//  MSSPAUtils.swift
//  mSSPA v02r01
//
//  Created by Karim Razak Soriano on 23/10/2018.
//  Copyright © 2018 Babel. All rights reserved.
//

import Foundation
import SystemConfiguration
import KeychainAccess

@objc public class MSSPAUtils: NSObject {

    static var APP_ID_PREFIX = "GRH9G7SS99"

    static let SALUD_RESPONDE_ID_APP = "msspa.app.000"
    static let SALUD_RESPONDE_LINK_APPLE_STORE = "itms-apps://itunes.apple.com/es/app/salud-responde/id681103926"

    /**
        Default value: GRH9G7SS99
    */
    @objc public static func setAppIdPrefix( _ appIdPrefix: String) {
        self.APP_ID_PREFIX = appIdPrefix
    }

   @objc public static func hasNetworkConnection() -> Bool {

        var flags = SCNetworkReachabilityFlags()
        guard let address = SCNetworkReachabilityCreateWithName(nil, "www.google.com") else {
            return false
        }

        let success = SCNetworkReachabilityGetFlags(address, &flags)
            && flags.contains(.reachable)
            && !flags.contains(.connectionRequired)

        return success
    }

    @objc public static func openSaludRespondeFromAppWithIdApp(_ idApp: String){

        if isInstalledAppWithIdApp(SALUD_RESPONDE_ID_APP){
            let urlScheme = "\(SALUD_RESPONDE_ID_APP)://fields?idApp=\(idApp)"
            openAppInAppleStore(urlScheme)
        }else{
            openAppInAppleStore(SALUD_RESPONDE_LINK_APPLE_STORE)
        }
    }

    @objc public static func openAppInAppleStore(_ urlApp: String){

        if let url = URL(string: urlApp),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    @objc public static func isInstalledAppWithIdApp(_ idApp: String) -> Bool{

        let urlScheme = "\(idApp)://"
        if let url = URL(string: urlScheme) {
            return UIApplication.shared.canOpenURL(url)
        }else{
            return false
        }
    }

    @objc public static func getIdDevice() -> String{
        if let uuid = UIDevice.current.identifierForVendor{
            return uuid.uuidString
        }else{
            return ""
        }
    }

    @objc public static func readIdDeviceMSSPAFromKeychain() -> String {

        let accessGroup = "\(APP_ID_PREFIX).mSSPA"
        let keychain = Keychain(service: "mSSPA", accessGroup: accessGroup)
        let idDeviceMSSPA = keychain["idDeviceMSSPA"]

        return idDeviceMSSPA ?? ""
    }

    @objc public static func saveIdDeviceMSSPAInKeychain(_ idDeviceMSSPA:String) {
        let accessGroup = "\(APP_ID_PREFIX).mSSPA"
        let keychain = Keychain(service: "mSSPA", accessGroup: accessGroup)
        keychain.accessibility(.afterFirstUnlock)["idDeviceMSSPA"] = idDeviceMSSPA
    }

    @objc public static func readQREncryptedFromKeychain() -> String {
        let accessGroup = "\(APP_ID_PREFIX).mSSPA"
        let keychain = Keychain(service: "mSSPA", accessGroup: accessGroup)
        return keychain["qr"] ?? ""
    }

    @objc public static func saveQREncryptedInKeychain(qrEncrypted: String) {
        let accessGroup = "\(APP_ID_PREFIX).mSSPA"
        let keychain = Keychain(service: "mSSPA", accessGroup: accessGroup)
        keychain.accessibility(.afterFirstUnlock)["qr"] = qrEncrypted
    }

    @objc public static func readDNIEncryptedFromKeychain() -> String {
        let accessGroup = "\(APP_ID_PREFIX).mSSPA"
        let keychain = Keychain(service: "mSSPA", accessGroup: accessGroup)
        return keychain["dni"] ?? ""
    }

    @objc public static func saveDNIEncryptedInKeychain(_ dniEncrypted: String) {
        let accessGroup = "\(APP_ID_PREFIX).mSSPA"
        let keychain = Keychain(service: "mSSPA", accessGroup: accessGroup)
        keychain.accessibility(.afterFirstUnlock)["dni"] = dniEncrypted
    }

    @objc public static func readHashPINFromKeychain() -> String {
        let accessGroup = "\(APP_ID_PREFIX).mSSPA"
        let keychain = Keychain(service: "mSSPA", accessGroup: accessGroup)
        return keychain["hashpin"] ?? ""
    }

    @objc public static func saveHashPINInKeychain(_ hashPIN: String) {
        let accessGroup = "\(APP_ID_PREFIX).mSSPA"
        let keychain = Keychain(service: "mSSPA", accessGroup: accessGroup)
        keychain.accessibility(.afterFirstUnlock)["hashpin"] = hashPIN
    }

    @objc public static func generateTokenOTP(tokenWeb: String) -> String {

        let seconds = Date().timeIntervalSince1970
        let milliseconds = seconds * 1000;
        let division = milliseconds / 30000;
        let rounded = (division + 0.5);

        let tokenOTP = String(format: "%@%i", arguments: [tokenWeb, rounded])

        let cstr = tokenOTP.cString(using: .utf8)
        var bytes = [UInt8](repeating: 0, count: 16)
        let result = UnsafeMutablePointer<UInt8>.allocate(capacity: bytes.count)
        result.initialize(from: &bytes, count: bytes.count)

        CC_MD5(cstr, CC_LONG(strlen(cstr)), result);

        return String(format: "%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                      arguments: [result[0], result[1], result[2], result[3],
                                  result[4], result[5], result[6], result[7],
                                  result[8], result[9], result[10], result[11],
                                  result[12], result[13], result[14], result[15]]).lowercased()
    }

    // Not kRNCryptorAES256Settings
    @objc public static func encryptAES256(text: String, key: String) -> String? {

        guard let data = text.data(using: .utf8) else {return nil}

        let encryptedData = RNCryptor.encrypt(data: data, withPassword: key)
        return encryptedData.base64EncodedString(options: .lineLength64Characters)
    }

    @objc public static func decryptAES256(textEnc: String, key: String) -> String? {

        guard let encryptedData = Data(base64Encoded: textEnc, options: .ignoreUnknownCharacters) else {return nil}

        do {
            let data = try RNCryptor.decrypt(data: encryptedData, withPassword: key)
            return String(data: data, encoding: .utf8)
        }
        catch{
            return nil
        }
    }

    @objc public static func getHashSHA2_512(_ string: String) -> String? {

        var digest = [UInt8](repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
        if let data = string.data(using: String.Encoding.utf8) {
            let value =  data as NSData
            CC_SHA512(value.bytes, CC_LONG(data.count), &digest)

        }
        var digestHex = ""
        for index in 0..<Int(CC_SHA512_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }

        return digestHex
    }

    @objc public static func getModelDevice() -> String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String {
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }

    @objc public static func getAuthorizationBySaludRespondeWithUrl(_ url: URL) -> String {

        var SRAuthorization = ""
        let components = URLComponents(url: url, resolvingAgainstBaseURL: false)

        if let comp = components, let params = comp.queryItems {
            params.forEach{ item in

                if item.name == "SRAuthorization" {
                    SRAuthorization  = (item.value ?? "").removingPercentEncoding ?? ""
                }
            }
        }
        return SRAuthorization
    }

    @objc public static func getExpiredDateBySaludRespondeWithUrl(_ url: URL) -> Date? {

        var SRExpiredDate: Date? = nil
        let components = URLComponents(url: url, resolvingAgainstBaseURL: false)

        if let comp = components, let params = comp.queryItems {
            params.forEach{ item in
                if item.name == "SRExpiredDate" {
                    if let expiredDate = item.value {
                        let format = DateFormatter()
                        format.dateFormat = "yyyyMMddHHmmss"
                        SRExpiredDate = format.date(from: expiredDate)
                    }
                }
            }
        }

        return SRExpiredDate
    }

    @objc public static func getPINBySaludRespondeWithUrl(_ url: URL) -> Bool {

        var SRPin = false
        let components = URLComponents(url: url, resolvingAgainstBaseURL: false)

        if let comp = components, let params = comp.queryItems {
            params.forEach{ item in
                if item.name == "SRPin", item.value == "true" {
                    SRPin = true
                }
            }
        }

        return SRPin
    }

    @objc public static func getAuthenticationTypeBySaludRespondeWithUrl(_ url: URL) -> Int {
        var SRType = 0
        let components = URLComponents(url: url, resolvingAgainstBaseURL: false)

        if let comp = components, let params = comp.queryItems {
            params.forEach{ item in
                if item.name == "SRTypeAut" {
                    if let value = item.value, let int = Int(value) {
                        SRType = int
                    }
                }
            }
        }
        return SRType
    }

    @objc public static func hasSecurityDevice() -> Bool {

        guard let secret = "Device has passcode set?".data(using: .utf8) else {
            return false
        }
        let attributes: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrService as String: "LocalDeviceServices",
                                    kSecAttrAccount as String: "NoAccount",
                                    kSecValueData as String: secret,
                                    kSecAttrAccessible as String: kSecAttrAccessibleWhenPasscodeSetThisDeviceOnly]

        let status = SecItemAdd(attributes as CFDictionary, nil)
        guard status == errSecSuccess else {
            return false
        }
        SecItemDelete(attributes as CFDictionary)
        return true
    }
}
